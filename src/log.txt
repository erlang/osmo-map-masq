1> {ok, IoDev} = file:open("/tmp/bin", read).
{ok,<0.33.0>}
2> {ok, Data} = file:read(IoDev, 255).
{ok,[98,129,129,72,4,104,34,66,102,107,26,40,24,6,7,0,17,
     134,5,1,1,1,160,13,96,11,161|...]}
3> asn1rt:decode('MAP','MapSpecificPDUs', Data).
{ok,{'begin',
        {'MapSpecificPDUs_begin',"h\"Bf",
            {'EXTERNAL',
                {syntax,{0,0,17,773,1,1,1}},
                asn1_NOVALUE,
                <<96,11,161,9,6,7,4,0,0,1,0,3,3>>},
            [{basicROS,
                 {invoke,
                     {'MapSpecificPDUs_begin_components_SEQOF_basicROS_invoke',
                         {present,27},
                         asn1_NOVALUE,
                         {local,4},
                         {'ProvideRoamingNumberArg',
                             [21,16,96,51,153,89,73,249],
                             [145,38,24,1,5,113],
                             [145,38,24,99,147,136,57|...],
                             [0,3,67,198],
                             {'ExternalSignalInfo','gsm-0408',[4,1|...],asn1_NOVALUE},
                             asn1_NOVALUE,asn1_NOVALUE,
                             [145,38|...],
                             [245|...],
                             asn1_NOVALUE,...}}}}]}}}
4> gen_udp:open([4243], [binary, inet]).
** exception error: no function clause matching inet_udp:getserv([4243])
     in function  gen_udp:open/2
5> gen_udp:open(4243, [binary, inet]).  
{ok,#Port<0.692>}
6> receive.    
* 1: syntax error before: '.'
6> receive
6> receive .
* 2: syntax error before: '.'
6> receive {udp, Sock, ClientIP, ClientPort, Packet}.
* 1: syntax error before: '.'
6> receive {udp, Sock, ClientIP, ClientPort, Packet} end.
* 1: syntax error before: 'end'
6> receive {udp, Sock, ClientIP, ClientPort, Packet} io:format("foo~n").
* 1: syntax error before: io
6> receive {udp, Sock, ClientIP, ClientPort, Packet} -> io:format("foo~n").
* 1: syntax error before: '.'
6> receive {udp, Sock, ClientIP, ClientPort, Packet} -> io:format("foo~n") end.
foo
ok
7> receive {udp, Sock, ClientIP, ClientPort, Packet} -> io:format("foo~n") end.
foo
ok
8> receive {udp, Sock, ClientIP, ClientPort, Packet} -> asn1rt:decode('MAP', 'MapSpecificPDUs', Packet) end.
{ok,{'begin',
        {'MapSpecificPDUs_begin',
            [0,0,128,0],
            {'EXTERNAL',
                {syntax,{0,4,0,0,1,0,1,3}},
                asn1_NOVALUE,
                <<128,17,96,15,128,2,7,128,161,9,6,7,4,0,0,1,0,32,...>>},
            [{basicROS,
                 {invoke,
                     {'MapSpecificPDUs_begin_components_SEQOF_basicROS_invoke',
                         {present,0},
                         asn1_NOVALUE,
                         {local,23},
                         {'UpdateGprsLocationArg',
                             [21,16,96,36,6,0,133,248],
                             [145,38,24,1,33,7,241],
                             [4,221,132,193,53],
                             asn1_NOVALUE,
                             {'SGSN-Capability',asn1_NOVALUE,asn1_NOVALUE,asn1_NOVALUE,
                                 'NULL',...},
                             asn1_NOVALUE,asn1_NOVALUE,asn1_NOVALUE,asn1_NOVALUE}}}},
             {basicROS,
                 {invoke,
                     {'MapSpecificPDUs_begin_components_SEQOF_basicROS_invoke',
                         {present,1},
                         asn1_NOVALUE,
                         {local,23},
                         asn1_NOVALUE}}},
             {basicROS,
                 {invoke,
                     {'MapSpecificPDUs_begin_components_SEQOF_basicROS_invoke',
                         {present,-1},
                         asn1_NOVALUE,
                         {local,23},
                         asn1_NOVALUE}}}]}}}
