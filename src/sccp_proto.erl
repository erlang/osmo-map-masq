% Erlang SCCP protocol interface 

% (C) 2010 by Harald Welte <laforge@gnumonks.org>
% (C) 2010 by On-Waves
%
% All Rights Reserved
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program; if not, write to the Free Software Foundation, Inc.,
% 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

-module(sccp_proto).
-author('Harald Welte <laforge@gnumonks.org>').
-compile(export_all).

-define(TIMEOUT, 1000).
-define(SRVNAME, c2342@localhost).

% synchronous wrapper around sccp_node.c
% FIXME: include a ref() to match call/result
split_udt_sync(UdtBinary) ->
	% send command to the C node
	{any, ?SRVNAME} ! {call, self(), {udt_split_cmd, UdtBinary}},
	receive
		{udt_split_res, SccpHdrBin, SccpPayldBin} ->
			{ok, SccpHdrBin, SccpPayldBin};
		{udt_split_err} ->
			{error, undefined}
	after
		?TIMEOUT ->
			{error, timeout}
	end.

% synchronous wrapper around sccp_node.c
% FIXME: include a ref() to match call/result
wrap_udt_sync(HdrBinary, PayloadBinary) ->
	% send command to the C node
	{any, c2342@localhost} ! {call, self(), {udt_wrap_cmd, HdrBinary, PayloadBinary}},
	receive
		{udt_wrap_res, SccpBinary} ->
			{ok, SccpBinary};
		{udt_wrap_err} ->
			{error, undefined}
	after
		?TIMEOUT ->
			{error, timeout}
	end.

