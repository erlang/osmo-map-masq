-module(omm_app).
-behaviour(application).
-export([start/2, stop/1]).

start(_Type, _Args) ->
	{ok, UdpPort} = application:get_env(?MODULE, udp_listen_port),
	{ok, IpaDstAddr} = application:get_env(?MODULE, ipa_connect_ip),
	{ok, IpaDstPort} = application:get_env(?MODULE, ipa_connect_port),
	{ok, IpaStreamID} = application:get_env(?MODULE, ipa_stream_id),
	osmo_map_masq:init(UdpPort, IpaDstAddr, IpaDstPort, IpaStreamID).
stop(_State) ->
	ok.
